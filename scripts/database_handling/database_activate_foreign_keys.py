"""
Python script container of database_activate_foreign_keys()
database_activate_foreign_keys()
    connection:
        The connection to the database
    cursor:
        The connection's cursor
"""
# Built-in imports
import sqlite3
# External librairies imports
# Custom librairies imports


def database_activate_foreign_keys(connection:sqlite3.Connection, cursor:sqlite3.Cursor) -> None:
    """
    Activates foreign_keys functionality
    Must be done with every connection:
        Sqlite3
    """
    cursor.execute(
        """
        PRAGMA foreign_keys = ON
        """
    )
    connection.commit()
    del cursor
    del connection
    return None