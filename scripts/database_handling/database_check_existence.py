"""
Python script container of database_check_existence()
database_check_existence()
    database_filepath:
        The filepath to the database to use
        examples:
            "./records.db"
            "/home/a/b/c/records.db"
        Will create file if needed
        Relative and absolute filepaths supported
        When only a filename is provided, will use relative "./" path
"""
# Built-in imports
import os
# External librairies imports
# Custom librairies imports


def database_check_existence(path_to_file:str, filename:str) -> None:
    """
    Checks for database existence and create it if it does not exist.
    """
    if not(filename in os.listdir(path=path_to_file)): #check database existence
        print(f"Database <{filename}> does not exist in folder <{path_to_file}>, creating database file...")
        with open(path_to_file+"/"+filename, "w"): #create database if it does not exist
            pass
        if filename in os.listdir(path=path_to_file): #check creation worked
            print(f"Database file <{filename}> has been created.")
        else:
            raise NotImplementedError("Unhandled Error: database creation seems not to have happened - In module: database_check_existence.py")
    del path_to_file
    del filename
    return None