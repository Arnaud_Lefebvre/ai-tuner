"""
Python script container of database_setup()
database_setup()
    connection
        sqlite3.Connection to the database
    cursor
        sqlite3.Cursor from the connection
"""
# Built-in imports
import sqlite3
import sys
# External librairies imports
# Custom librairies imports


def database_tables_setup(connection:sqlite3.Connection, cursor:sqlite3.Cursor) -> None:
    """
    Sets up database standard tables
    """
    # Tables name and column
    table_list = [
        {
            "name": "main",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "score REAL NOT NULL",
                "execution_time_ms REAL NOT NULL",
                "model_param_group_id INTEGER NOT NULL",
                "dataset_column_group_id INTEGER NOT NULL",
                "serie_id INTEGER NOT NULL"
            ],
            "constraints": [
                "CHECK ((1 >= score) AND (score >= 0))",
                "FOREIGN KEY (model_param_group_id) REFERENCES model_param_group_assoc(id) ON DELETE CASCADE ON UPDATE CASCADE",
                "FOREIGN KEY (dataset_column_group_id) REFERENCES dataset_column_group_assoc(id) ON DELETE CASCADE ON UPDATE CASCADE",
                "FOREIGN KEY (serie_id) REFERENCES serie_ref(id) ON DELETE CASCADE ON UPDATE CASCADE"
            ]
        },
        {
            "name": "serie_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "datetime_at_runtime_start TEXT NOT NULL",
                "mode TEXT NOT NULL"
            ],
            "constraints": [
                "CHECK (substr(datetime_at_runtime_start, 0, 20) = strftime('%Y-%m-%d %H:%M:%S',datetime_at_runtime_start))"
            ]
        },
        {
            "name": "mode_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "name TEXT NOT NULL"
            ],
            "constraints": [
                "CHECK (name IN ('param', 'column', 'dummy'))"
            ]
        },
        {
            "name": "model_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "source TEXT NOT NULL",
                "name TEXT NOT NULL"
            ],
            "constraints": [
                "CONSTRAINT unique_model UNIQUE(source, name)"
            ]
        },
        {
            "name": "model_param_group_assoc",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "model_id INTEGER NOT NULL",
                "param_group_id INTEGER NOT NULL"
            ],
            "constraints": [
                "CONSTRAINT unique_model_param_group_assoc UNIQUE(model_id, param_group_id)",
                "FOREIGN KEY (model_id) REFERENCES model_ref(id) ON DELETE CASCADE ON UPDATE CASCADE",
                "FOREIGN KEY (param_group_id) REFERENCES param_group_ref(id) ON DELETE CASCADE ON UPDATE CASCADE"
            ]
        },
        # This table's sole purpose is to grant AUTOINCREMENT functionality on this ID (INTEGER PRIMARY KEY is REQUIRED for that with Sqlite3)
        {
            "name": "param_group_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
            ],
            "constraints": [
                
            ]
        },
        {
            "name": "param_assoc",
            "columns": [
                "param_group_id INTEGER NOT NULL",
                "param_id INTEGER NOT NULL"
            ],
            "constraints": [
                "PRIMARY KEY (param_group_id, param_id)",
                "FOREIGN KEY (param_group_id) REFERENCES param_group_ref(id) ON DELETE CASCADE ON UPDATE CASCADE",
                "FOREIGN KEY (param_id) REFERENCES param_ref(id) ON DELETE CASCADE ON UPDATE CASCADE"
            ]
        },
        {
            "name": "param_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "name TEXT NOT NULL",
                "value TEXT NOT NULL"
            ],
            "constraints": [
                "CONSTRAINT unique_param UNIQUE(name, value)"
            ]
        },
        {
            "name": "dataset_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "name TEXT NOT NULL",
                "line_count INTEGER NOT NULL",
                "test_train_split REAL NOT NULL"
            ],
            "constraints": [
                "CHECK ((0.9 >= test_train_split) AND (test_train_split >= 0.1))",
                "CONSTRAINT unique_dataset UNIQUE(name, line_count, test_train_split)"
            ]
        },
        {
            "name": "dataset_column_group_assoc",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "dataset_id INTEGER NOT NULL",
                "column_group_id INTEGER NOT NULL"
            ],
            "constraints": [
                "CONSTRAINT unique_dataset_column_group_assoc UNIQUE(dataset_id, column_group_id)",
                "FOREIGN KEY (dataset_id) REFERENCES dataset_ref(id) ON DELETE CASCADE ON UPDATE CASCADE",
                "FOREIGN KEY (column_group_id) REFERENCES column_group_ref(id) ON DELETE CASCADE ON UPDATE CASCADE"
            ]
        },
        # This table's sole purpose is to grant AUTOINCREMENT functionality on this ID (INTEGER PRIMARY KEY is REQUIRED for that with Sqlite3)
        {
            "name": "column_group_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
            ],
            "constraints": [

            ]
        },
        {
            "name": "column_assoc",
            "columns": [
                "column_group_id INTEGER NOT NULL",
                "column_id INTEGER NOT NULL"
            ],
            "constraints": [
                "PRIMARY KEY (column_group_id, column_id)",
                "FOREIGN KEY (column_group_id) REFERENCES column_group_ref(id) ON DELETE CASCADE ON UPDATE CASCADE",
                "FOREIGN KEY (column_id) REFERENCES column_ref(id) ON DELETE CASCADE ON UPDATE CASCADE"
            ]
        },
        {
            "name": "column_ref",
            "columns": [
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL",
                "name TEXT NOT NULL",
                "data_type TEXT"
            ],
            "constraints": [
                "CONSTRAINT unique_column UNIQUE(name, data_type)"
            ]
        }
    ]

    # Check database's data model
    #   by comparing tables' names and column counts
    #
    # Setup boolean column_change_occured_bool
    column_change_occured_bool = False
    for table in table_list: #for each table
        table_name = table["name"]
        table_column_count = len(table["columns"]) #get column count
        # Check for table column count
        cursor.execute(
            f"""
            SELECT name FROM PRAGMA_TABLE_INFO('{table_name}')
            """
        )
        fetched_column_count = len([x[0] for x in cursor.fetchall()])
        # Set boolean if a change occured
        if not(fetched_column_count == table_column_count):
            column_change_occured_bool = True
            break #and stop, no need to check further
            #(because of relationships, better to rewrite entire database..?)
            #section might be bad but gotta learn on <ON DELETE CASCADE> mechanics
    del table_name
    del table_column_count
    del table
    # Drop tables if a change occured
    if column_change_occured_bool and (not(fetched_column_count == 0)):
        print("A change seems to have happened in the data model.")
        print("WILL DELETE TABLES...")
        y_n = str(input("Do you wish to continue with database erasing..? (Y/y OR any key):"))
        if y_n.lower() == "y":
            print("DELETING TABLES...")
            for table in table_list:
                table_name = table["name"]
                try:
                    cursor.execute(
                        f"""
                        DROP TABLE IF EXISTS {table_name}
                        """
                    )
                except Exception as _error:
                    print(_error)
                    del _error
            connection.commit()
            print("Tables now will be recreated")
            del table_name
            del table
        else:
            print("Database setup not identical...")
            print("Exiting...")
            sys.exit()
        del y_n
    del column_change_occured_bool
    del fetched_column_count

    # Creates any table that does not exist
    #
    for table in table_list:
        table_name = table["name"]
        table_columns = table["columns"]
        table_constraints = table["constraints"]
        # Concatenates every column & constraint to feed SQL Query
        table_arguments = ",".join(table_columns + table_constraints)
        cursor.execute(
            f"""
            CREATE TABLE IF NOT EXISTS {table_name}(
                {table_arguments}
            )
            """
        )
    connection.commit()
    del table_name
    del table_columns
    del table_constraints
    del table_arguments
    del table
    del table_list
    del cursor
    del connection
    return None