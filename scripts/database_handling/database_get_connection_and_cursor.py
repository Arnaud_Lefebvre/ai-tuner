"""
Python script container of database_get_connection_and_cursor()
database_get_connection_and_cursor()
    database_filepath:
        The filepath to the database to use
"""
# Built-in imports
import sqlite3
from typing import Sequence
# External librairies imports
# Custom librairies imports


def database_get_connection_and_cursor(database_filepath: str) -> Sequence:
    """
    Connects to database and sets up cursor
    """
    connection = sqlite3.connect(database_filepath)
    cursor = connection.cursor()
    del database_filepath
    return connection, cursor