"""
Python script container of database_setup()
database_setup()
    Will only setup variables and run other functions
Functions:
    database_check_existence()
        Checks that database exists, creates it if not
    database_get_connection_and_cursor()
        Gets and returns connection and cursor for database
        Supported:
            Sqlite3
    database_activate_foreign_keys()
        Activates database's foreign keys functionality
        Required with:
            Sqlite3
    database_setup()
        Check that the data model saved in var <table_list> is present in database
        Absence:
            Will create tables
        Mismatch: 
            Will ask user for tables deletion:
                Yes:
                    Drops existing tables before creating them all
                No:
                    Not supported
        Match:
            Does nothing
        Supported:
            Sqlite3
Variables:
    database_filepath:
        The filepath to the database to use
        examples:
            "./records.db"
            "/home/a/b/c/records.db"
        Relative and absolute filepaths supported
        When only a filename is provided, will use relative "./" path
"""
# Built-in imports
from typing import Sequence
# External librairies imports
# Custom librairies imports
from scripts.database_handling import database_activate_foreign_keys
from scripts.database_handling import database_check_existence
from scripts.database_handling import database_get_connection_and_cursor
from scripts.database_handling import database_tables_setup


def database_main(database_filepath: str="records.db") -> Sequence:
    """
    Runs everything related to database setup
        Checks that files exists
        Connects and add cursor
        Activates foreign keys
        Checks tables, (re)creates them when required
    Returns sqlite3.Connection, sqlite3.Cursor
    """
    # Database_filepath check
    #
    split_database_filepath = database_filepath.split("/")
    # If there is only a filename and no path
    if (len(split_database_filepath)==1) or ((len(split_database_filepath)==2)and(split_database_filepath[0]=="")):
        path_to_file = "."
    else: #else get the path to file
        path_to_file = "/".join(split_database_filepath[:-1])
    filename = split_database_filepath[-1]
    database_filepath = path_to_file+"/"+filename #in case "/..." was provided
    del split_database_filepath
    #
    database_check_existence.database_check_existence(path_to_file=path_to_file, filename=filename)
    connection, cursor = database_get_connection_and_cursor.database_get_connection_and_cursor(database_filepath=database_filepath)
    database_activate_foreign_keys.database_activate_foreign_keys(connection=connection, cursor=cursor)
    database_tables_setup.database_tables_setup(connection=connection, cursor=cursor)
    del database_filepath
    del path_to_file
    del filename
    return (connection, cursor)