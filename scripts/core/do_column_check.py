"""

"""
# Built-in imports
import itertools
import sqlite3
from typing import Iterable, List
# External librairies imports
import pandas
# Custom librairies imports


def do_column_check(
    mode: int,
    data: pandas.DataFrame,
    data_X_columns: Iterable[str],
    data_y_column: str
    ) -> List[List]:
    """
    
    """
    # Get column list according to mode
    if mode == 2: #mode check every column
        columns = []
        for column in data.columns:
            if (not(column == "Unnamed: 0")) and (not(column == data_y_column)):
                columns.append(column)
    elif mode == 1: # mode check selected columns
        columns = data_X_columns
    del mode
    del data_X_columns
    # Get every column - removing index and target
    column_combination_list = []
    # Use itertools to save to list every combination
    if len(columns) <= 20: #if processing would not be so demanding
        for combination_length in range(1, len(columns)+1):
            column_combination_list.extend([tuple(x) for x in itertools.combinations(columns, combination_length)])
    else: #else if it probably would be, only save to list every single column && entirety && selected columns
        column_combination_list.append(columns)
        column_combination_list.extend(columns)
        column_combination_list.append(data_X_columns)
        column_combination_list = list(set(column_combination_list)) #trash possible doubles
    
    return column_combination_list