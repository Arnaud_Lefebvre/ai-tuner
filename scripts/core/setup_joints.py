"""

"""
# Built-in imports
from os import pardir
import sqlite3
from typing import Dict, Iterable, Tuple
# External librairies imports
from sklearn.dummy import DummyClassifier
from sklearn.tree import DecisionTreeClassifier
# Custom librairies imports
from scripts.core import param_lists_to_combinations_list


def do_all_join_insert(
    connection: sqlite3.Connection,
    cursor: sqlite3.Cursor,
    data_name: str,
    data_line_count: int,
    test_train_splits: Iterable[float],
    column_combination_list: Iterable[str],
    model_dict: Iterable,
    param_dict: Iterable
    ):
    """
    Need to fetch / insert
        serie_id
        model_param_group_id
            for each model params set
        dataset_column_group_id
            for each dataset columns set

    IN
        every
            model
            param_group
            dataset
            column_group
    OUT
        every
            model, param_group && associated ID
            dataset, column_group && associated ID
        ids_reference = {
            (models): {
                (param_group): id,
                (param_group): id,
                (...): ...
            },
            (datasets): {
                (column_group): id,
                (column_group): id,
                (...): ...
            }
        }
    """
    ids_reference = {}
    for test_train_split in test_train_splits:
        # get dataset_id
        cursor.execute(
            f"""
            SELECT
                dataset_ref.id
            FROM
                dataset_ref
            WHERE
                    (dataset_ref.name = '{data_name}')
                AND
                    (dataset_ref.line_count = {data_line_count})
                AND
                    (dataset_ref.test_train_split =  {test_train_split})
            """
        )
        fetched = cursor.fetchall()
        result_count = len(fetched)
        if result_count >= 2:
            raise NotImplementedError("Unhandled supposedly impossible case happened")
        elif result_count == 1: #id exists, save id
            dataset_id = fetched[0][0]
        else: #id does not exist, insert values then save id
            cursor.execute(
                f"""
                INSERT INTO dataset_ref
                    (name, line_count, test_train_split)
                VALUES
                    ('{data_name}', {data_line_count}, {test_train_split})
                """
            )
            dataset_id = cursor.lastrowid
        del fetched
        del result_count
        dataset_key = (data_name, data_line_count, test_train_split)
        if not(dataset_key in ids_reference.keys()):
            ids_reference[dataset_key] = {}

        for column_combination in column_combination_list:
            column_ids_list = []
            # Check column ref existence and create if needed
            for column_name in column_combination:
                cursor.execute(
                    f"""
                    SELECT
                        id
                    FROM
                        column_ref
                    WHERE
                        name = '{column_name}'
                    """
                )
                fetched = cursor.fetchall()
                result_count = len(fetched)
                if result_count >= 2:
                    raise NotImplementedError("Unhandled supposedly impossible case happened")
                elif result_count == 1: #id exists, save id
                    column_id = fetched[0][0]
                else: #id does not exist, insert values then save id
                    cursor.execute(
                        f"""
                        INSERT INTO column_ref
                            (name)
                        VALUES
                            ('{column_name}')
                        """
                    )
                    column_id = cursor.lastrowid
                column_ids_list.append(column_id)
                del column_id
                del result_count
                del fetched
            del column_name
            
            # Prepare query
            column_ids_concatenated = ",".join([str(x) for x in column_ids_list])
            count_to_get = len(column_ids_list)
            # Query
            cursor.execute(
                f"""
                SELECT
                    column_group_id, column_id
                FROM
                    column_assoc
                WHERE
                    column_group_id IN (
                        SELECT
                            column_group_id
                        FROM
                            column_assoc
                        WHERE
                            column_group_id IN (
                                SELECT
                                    column_group_id
                                FROM
                                    column_assoc
                                WHERE
                                    column_id IN ({column_ids_concatenated})
                            )
                        GROUP BY
                            column_group_id
                        HAVING
                            COUNT(column_id) = {count_to_get}
                    )
                ORDER BY
                    column_group_id
                """
            )
            del count_to_get
            fetched = cursor.fetchall()
            ###### find right id
            gid = None
            pgid = None
            lll=[]
            ll=[]
            for item in fetched:
                if gid is None:
                    gid = item[0]
                    ll.append(item[1])
                else:
                    pgid = gid
                    gid = item[0]
                if gid == pgid:
                    ll.append(item[1])
                elif pgid is not None:
                    lll.append((pgid,ll))
                    ll = []
                    ll.append(item[1])
                del item
            lll.append((gid,ll))
            result_count = 0
            column_group_id = None
            for item in lll:
                if set(item[1]) == set(column_ids_list):
                    result_count += 1
                    column_group_id = item[0]
            ###############
            # result_count = len(fetched)
            # result_count= 1
            if result_count >= 2:
                raise NotImplementedError("Unhandled supposedly impossible case happened")
            elif result_count == 1: #column_group_id exists, save id
                column_group_id = fetched[0][0]
            else: #column_group_id does not exist, insert values then save id
                # Get a fresh column_group_id
                print("GOT THERE")
                cursor.execute(
                    """
                    INSERT INTO column_group_ref DEFAULT VALUES
                    """
                )
                column_group_id = cursor.lastrowid
                # Insert relationships into column_assoc
                list_to_pass = []
                for id in column_ids_list:
                    list_to_pass.append((column_group_id, id))
                list_to_pass = list(set(list_to_pass))
                del id
                cursor.executemany(
                    f"""
                    INSERT INTO
                        column_assoc (column_group_id, column_id)
                    VALUES
                        (?, ?)
                    """,
                    list_to_pass
                )
                del list_to_pass
            del fetched
            del result_count
            # Now get the dataset_column_group_assoc id
            cursor.execute(
                f"""
                SELECT
                    id
                FROM
                    dataset_column_group_assoc
                WHERE
                        (dataset_id = {dataset_id})
                    AND
                        (column_group_id = {column_group_id})
                """
            )
            fetched = cursor.fetchall()
            result_count = len(fetched)
            if result_count >= 2:
                raise NotImplementedError("Unhandled supposedly impossible case happened")
            elif result_count == 1: #dataset_column_group_assoc_id exists, save id
                dataset_column_group_assoc_id = fetched[0][0]
            else: #dataset_column_group_assoc_id does not exist, insert values then save id
                cursor.execute(
                    f"""
                    INSERT INTO dataset_column_group_assoc (
                        dataset_id, column_group_id
                    )
                    VALUES
                        ({dataset_id}, {column_group_id})
                    """
                )
                dataset_column_group_assoc_id = cursor.lastrowid
            del fetched
            del result_count

            column_group_key = tuple(column_combination)
            ids_reference[dataset_key][column_group_key] = dataset_column_group_assoc_id
            del column_group_key














    for model_source, models_list in model_dict.items():
        for model in models_list:
            model_name = str(model)[:-2]
            # get model_id
            try: #on no result sqlite3 will not possibly handle this case - even after rewriting otherwise
                cursor.execute(
                    f"""
                    SELECT
                        id
                    FROM
                        model_ref
                    WHERE
                            (source = '{model_source}')
                        AND
                            (name = '{model_name}')
                    """
                )
                fetched = cursor.fetchall()
                result_count = len(fetched)
            except sqlite3.OperationalError:
                result_count = 0
            if result_count >= 2:
                raise NotImplementedError("Unhandled supposedly impossible case happened")
            elif result_count == 1: #id exists, save id
                model_id = fetched[0][0]
            else: #id does not exist, insert values then save id
                cursor.execute(
                    f"""
                    INSERT INTO model_ref
                        (source, name)
                    VALUES
                        ('{model_source}', '{model_name}')
                    """
                )
                model_id = cursor.lastrowid
            del fetched
            del result_count
            model_key = (model_source, model_name)
            if not(model_key in ids_reference.keys()):
                ids_reference[model_key] = {}


            parameters_dict = param_dict[model_source][model_name]
            parameters_names_list = list(parameters_dict.keys())
            parameters_values_lists_list = list(parameters_dict.values())
            parameters_combinations = param_lists_to_combinations_list.param_lists_to_combinations_list(parameters_values_lists_list)
            # Do param_ref
            param_ids_dict = {}
            # param_ids_list = []
            for parameter_name, parameter_values_list in parameters_dict.items():
                param_ids_dict[parameter_name] = {}
                for parameter_value in parameter_values_list:
                    cursor.execute(
                        f"""
                        SELECT
                            id
                        FROM
                            param_ref
                        WHERE
                                name = '{parameter_name}'
                            AND
                                value = '{str(parameter_value)}'
                        """
                    )
                    fetched = cursor.fetchall()
                    result_count = len(fetched)
                    if result_count >= 2:
                        raise NotImplementedError("Unhandled supposedly impossible case happened")
                    elif result_count == 1: #id exists, save id
                        param_id = fetched[0][0]
                    else: #id does not exist, insert values then save id
                        cursor.execute(
                            f"""
                            INSERT INTO param_ref
                                (name, value)
                            VALUES
                                ('{parameter_name}', '{parameter_value}')
                            """
                        )
                        param_id = cursor.lastrowid
                    # param_ids_list.append(param_id)
                    param_ids_dict[parameter_name][parameter_value] = param_id
                    del param_id
                    del result_count
                    del fetched

            # Do combinations associations
            for parameters_combination in parameters_combinations:
                param_ids_list = []
                for p_index in range(len(parameters_combination)):
                    param_id = param_ids_dict[parameters_names_list[p_index]][parameters_combination[p_index]]
                    param_ids_list.append(param_id)
                # get ids

                # Prepare query
                param_ids_concatenated = ",".join([str(x) for x in param_ids_list])
                count_to_get = len(param_ids_list)
                # Query
                try: #on no result sqlite3 will not possibly handle this case - even after rewriting otherwise
                    cursor.execute(
                        f"""
                        SELECT
                            param_group_id, param_id
                        FROM
                            param_assoc
                        WHERE
                            param_group_id IN (
                                SELECT
                                    param_group_id
                                FROM
                                    param_assoc
                                WHERE
                                    param_group_id IN (
                                        SELECT
                                            param_group_id
                                        FROM
                                            param_assoc
                                        WHERE
                                            param_id IN ({param_ids_concatenated})
                                    )
                                GROUP BY
                                    param_group_id
                                HAVING
                                    COUNT(param_id) = {count_to_get}
                            )
                        ORDER BY
                            param_group_id
                        """
                    )
                    ###### find right id
                    fetched = cursor.fetchall()
                    gid = None
                    pgid = None
                    lll=[]
                    ll=[]
                    for item in fetched:
                        if gid is None:
                            gid = item[0]
                            ll.append(item[1])
                        else:
                            pgid = gid
                            gid = item[0]
                        if gid == pgid:
                            ll.append(item[1])
                        elif pgid is not None:
                            lll.append((pgid,ll))
                            ll = []
                            ll.append(item[1])
                        del item
                    lll.append((gid,ll))
                    result_count = 0
                    param_group_id = None
                    for item in lll:
                        if set(item[1]) == set(param_ids_list):
                            result_count += 1
                            param_group_id = item[0]
                    ###############
                    # fetched = cursor.fetchall()
                    # result_count = len(fetched)
                except sqlite3.OperationalError:
                    result_count = 0
                if result_count >= 2:
                    raise NotImplementedError("Unhandled supposedly impossible case happened")
                elif result_count == 1: #param_group_id exists, save id
                    param_group_id = fetched[0][0]
                else: #param_group_id does not exist, insert values then save id
                    # Get a fresh param_group_id
                    print("GOT HERE")
                    cursor.execute(
                        """
                        INSERT INTO param_group_ref DEFAULT VALUES
                        """
                    )
                    param_group_id = cursor.lastrowid
                    # Insert relationships into param_assoc
                    list_to_pass = []
                    for id in param_ids_list:
                        list_to_pass.append((param_group_id, id))
                        del id
                    list_to_pass = list(set(list_to_pass))
                    cursor.executemany(
                        f"""
                        INSERT INTO
                            param_assoc (param_group_id, param_id)
                        VALUES
                            (?, ?)
                        """,
                        list_to_pass
                    )
                    del list_to_pass
                del fetched
                del result_count
                # Now get the model_param_group_assoc id
                cursor.execute(
                    f"""
                    SELECT
                        id
                    FROM
                        model_param_group_assoc
                    WHERE
                            (model_id = {model_id})
                        AND
                            (param_group_id = {param_group_id})
                    """
                )
                fetched = cursor.fetchall()
                result_count = len(fetched)
                if result_count >= 2:
                    raise NotImplementedError("Unhandled supposedly impossible case happened")
                elif result_count == 1: #dataset_column_group_assoc_id exists, save id
                    model_param_group_assoc_id = fetched[0][0]
                else: #dataset_column_group_assoc_id does not exist, insert values then save id
                    cursor.execute(
                        f"""
                        INSERT INTO model_param_group_assoc (
                            model_id, param_group_id
                        )
                        VALUES
                            ({model_id}, {param_group_id})
                        """
                    )
                    model_param_group_assoc_id = cursor.lastrowid
                del fetched
                del result_count

                param_group_key = tuple(parameters_combination)
                ids_reference[model_key][param_group_key] = model_param_group_assoc_id
                del param_group_key


    connection.commit()
    return ids_reference