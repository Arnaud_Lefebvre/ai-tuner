"""

"""
# Built-in imports
import itertools
# External librairies imports
# Custom librairies imports


def param_lists_to_combinations_list(*args):
    combinations_list = itertools.product(*list(*args))
    return combinations_list