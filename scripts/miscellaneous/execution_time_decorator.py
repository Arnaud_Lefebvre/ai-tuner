"""
Decorator function to show general execution times
"""
# Built-in imports
import time
from datetime import datetime
# External librairies imports
# Custom librairies imports


def execution_time_decorator(section_name, program_start_time, line_number):
    def inner_decorator(function):
        """
        Decorator function to show general execution times
        """
        # Lenght of prints
        lenght = 80
        # Sets-up message to be printed
        def print_message(job_start_time:time.time, name:str, end:bool=False) -> str:
            """
            Returns message to be printed
            """
            start_current_time = str(round(time.time() - program_start_time, 2))
            section_current_time = str(round(time.time() - job_start_time, 2))
            del job_start_time
            message = \
                " Start of "+section_name+": \n\t" \
                + "line: "+str(line_number)+" - "+name+"() \n\t" \
                + "on "+str(datetime.now())+" \n\t" \
                + start_current_time+"s after start of program \n\t" \
                + section_current_time+"s after start of section "
            del name
            del start_current_time
            del section_current_time
            if end == True:
                message = message.replace("Start","End")
            del end
            if len(message) < lenght:
                message = message.replace("\n","|").replace("\t"," ")
            while len(message)<lenght:
                message = "-"+message+"-"
            if len(message)>lenght:
                message = message[1:]
            return message
        # Decorator core
        def inner(*args, **kwargs):
            name = function.__name__
            job_start_time = time.time()
            print('o'*lenght)
            print(print_message(job_start_time, name, end=False))
            print('o'*lenght)
            function(*args, **kwargs)
            print('x'*lenght)
            print(print_message(job_start_time, name, end=True))
            print('x'*lenght)
            del name
            del job_start_time
        del function
        del lenght
        return inner
    del section_name
    del program_start_time
    del line_number
    return inner_decorator