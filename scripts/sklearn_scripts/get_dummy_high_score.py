"""

"""
# Built-in imports
import time
from typing import Iterable
# External librairies imports
import pandas
from sklearn.dummy import DummyClassifier
# Custom librairies imports


def get_dummy_high_score(
    X_train: pandas.DataFrame,
    X_test: pandas.Series,
    y_train: pandas.DataFrame,
    y_test: pandas.Series,
    dummy_scores_repeats: int,
    dummy_strategies: Iterable[str],
    ) -> float:
    """
    
    """
    if (dummy_scores_repeats<=0) or (len(dummy_strategies)==0):
        return None
    highest_repeat_score_tuple_list = []
    for _ in range(dummy_scores_repeats):
        # Setup a list of scores per strategy: [(score, strategy), ...]
        score_per_strategy_list = []
        for strategy in dummy_strategies:
            if strategy == "constant": #constant case requires additional argument hence this if-else
                full_serie = y_train.append(y_test)
                most_frequent_value = full_serie.value_counts().index[0]
                del full_serie
                dummy = DummyClassifier(strategy=strategy, constant=most_frequent_value)
                del most_frequent_value
            else:
                dummy = DummyClassifier(strategy=strategy)
            # Train, test and save score & strategy to variable
            start_time = time.time()
            dummy.fit(X_train, y_train)
            score = dummy.score(X_test, y_test)
            exec_time = time.time() - start_time
            score_per_strategy_list.append((score, strategy, exec_time))
        # Get the highest score tuple
        highest_repeat_score_tuple = max(score_per_strategy_list, key=lambda x:x[0])
        highest_repeat_score_tuple_list.append(highest_repeat_score_tuple)
    del _
    del X_train
    del X_test
    del y_train
    del y_test
    del dummy_scores_repeats
    del dummy_strategies
    del highest_repeat_score_tuple
    del score_per_strategy_list
    del strategy
    del dummy
    del score
    del start_time
    del exec_time
    highest_score_tuple = max(highest_repeat_score_tuple_list, key=lambda x:x[0])
    del highest_repeat_score_tuple_list
    return highest_score_tuple