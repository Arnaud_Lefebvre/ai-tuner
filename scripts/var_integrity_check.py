"""
Storage for functions allowing variable integrity check
"""
# Built-in imports
# External librairies imports
# Custom librairies imports
import sys
from typing import Iterable


def check_test_sizes(test_sizes):
    """
    Checks var test_sizes value integrity
    """
    def do_rounding_and_limits_check(x):
        x = round(x, 1)
        if x <= 0:
            x = 0.1
        if x >= 1:
            x = 0.9
        return x
    # Check is Iterable
    if (not isinstance(test_sizes, Iterable)) or (isinstance(test_sizes, str)):
        try:
            test_sizes = float(test_sizes) #see if it contains a single valid value
        except ValueError: #if not, break program execution
            print(f"Invalid value ({test_sizes}) has been given to variable: <test_sizes>")
            print("Exiting...")
            sys.exit()
        test_sizes = do_rounding_and_limits_check(test_sizes)
        test_sizes = [test_sizes] #else turn that value to float
        return test_sizes
    else:
        test_sizes = list(test_sizes) #make sure it is not a kind of iterable that does not support round &or map
        elements_to_remove = []
        for test_size in test_sizes: #check every value's integrity
            try:
                test_size = float(test_size)
            except ValueError:
                print(f"Invalid value <{test_size}> has been given to variable: <test_sizes>")
                print("Invalid value will be ignored...")
                elements_to_remove.append(test_size) #save them to list
        for element_to_remove in elements_to_remove: #remove invalid values from test_sizes
            test_sizes.remove(element_to_remove)
        test_sizes = list(map(do_rounding_and_limits_check, test_sizes)) #check every numeric value
        test_sizes = list(set(test_sizes)) #remove any double
        return test_sizes