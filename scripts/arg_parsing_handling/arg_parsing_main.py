"""
Python script container of arg_parsing unitary cases to treat
Arguments:
    -d --datapath
        filepath to dataset
    -D --dataurl
        URL to download dataset from
    -c -columnscheck
        column benchmark option
Supported:
    Single valid data file or url argument
    Formats:
        .csv
        .json
"""
# Built-in imports
import argparse
import textwrap
from typing import Tuple
# External librairies imports
import pandas
# Custom librairies imports
from scripts.arg_parsing_handling import arg_parsing_switcher


def arg_parsing_main() -> Tuple[pandas.DataFrame, str, bool]:
    # Script execution options for feeding data
    parser = argparse.ArgumentParser(
        description="Program requires a dataset to function"
        "A single dataset must be passed usign -d(local filepath) or -D(dataset URL)"
        "Supported dataset formats are: csv, json",
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        "-D",
        "--dataurl",
        type=str,
        default=None,
        help="Str\n"
        "Pass a valid URL to download data from."
    )
    parser.add_argument(
        "-d",
        "--datapath",
        type=str,
        default=None,
        help="Str\n"
        "Pass a valid filepath to get data from."
    )
    parser.add_argument(
        "-c",
        "--columnscheck",
        action="count",
        default=0,
        help="-cc check every column\n"
        "-c: check selected columns\n"
        "empty: no column check\n"
        "Do a column benchmark (only)"
    )
    args = parser.parse_args()
    # Use functions to extract data from scripts arguments, then return it
    del parser
    data, data_name = arg_parsing_switcher.arg_parsing_case_switcher(
            bool(args.dataurl),
            bool(args.datapath)
    )(args.dataurl, args.datapath)
    # if args.columnscheck == "extra":
    #     column_check = "extra"
    # elif args.columnscheck == "only":
    #     column_check = "only"
    # else:
    #     column_check = None
    column_check = args.columnscheck
    del args
    return (data, data_name, column_check)