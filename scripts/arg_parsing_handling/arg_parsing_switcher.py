"""
Python script container of arg_parsing switch case function
arg_parsing_case_switcher()
    dataurl:
        bool(args.dataurl)
    datapath
        bool(args.datapath)
"""
# Built-in imports
from types import FunctionType
# External librairies imports
# Custom librairies imports
from scripts.arg_parsing_handling import arg_parsing_cases


def arg_parsing_case_switcher(dataurl,datapath) -> FunctionType:
    """
    Returns appropriate function to apply according to arguments passed when running Python script
    """
    switcher = {
        dataurl and datapath:
            arg_parsing_cases.arg_parsing_multiple_args,

        not(dataurl) and not(datapath):
            arg_parsing_cases.arg_parsing_no_args,

        dataurl and (not datapath):
            arg_parsing_cases.arg_parsing_dataurl,

        not(dataurl) and datapath:
            arg_parsing_cases.arg_parsing_datapath
    }
    return switcher.get(True)