"""
Python script container of arg_parsing unitary cases to treat
arg_parsing_no_args()
    Case no arguments were passed
arg_parsing_multiple_args()
    Case multiple arguments were passed (Not supported (yet))
arg_parsing_dataurl
    Case an URL was passed
arg_parsing_datapath
    Case a filepath was passed
"""
# Built-in imports
import io
import re
import sys
from typing import Tuple
# External librairies imports
import pandas
import requests
# Custom librairies imports


def arg_parsing_no_args(_, __) -> sys.exit:
    """
    Handles case no args passed 
    """
    del _
    del __
    print("Please pass data as argument. (you may run script appending --help to see options)")
    print("Exiting...")
    sys.exit()
def arg_parsing_multiple_args(_, __) -> sys.exit:
    """
    Handles case multiple args passed
    """
    del _
    del __
    print("Multiple datasets functionality is not implemented. Please feed a single dataset.")
    print("Exiting...")
    sys.exit()
def arg_parsing_dataurl(dataurl, _) -> Tuple[pandas.DataFrame, str]:
    """
    When a data URL was provided, fetches and returns data
    """
    del _
    print("URL passed as data, requesting URL...")
    response = requests.get(dataurl)
    # Loop-try on different reading options
    for reading in [
        response.text,
        response.content,
        io.StringIO(response.text)
        ]:
        # Loop-try on different formats
        for option in [
            pandas.read_csv,
            pandas.read_json
            ]:
            try:
                # Do the try
                data = option(reading)
                data_name = re.split(r"/|\\", dataurl)[-1]
                if len(data) <= 2: #if try worked, check result integrity - on fail, Raise handled error
                    raise ValueError
                else:
                    break #check passed, data should be valid - break try-loop and go to return
            except (ValueError, OSError):
                pass
            except Exception as _error:
                # If an uncatched error happened, log it to file
                filename = "error.logs"
                print(f"An unhandled error occured, recording error details in file: <{filename}>")
                with open(filename, "a") as error_file:
                    error_file.write(str(_error) + "\n" + str(sys.exc_info()) + "\narg_parsing_cases.py" + "\n-------------------------\n\n")
                del _error
                del error_file
                del filename
    del response
    del reading
    del option
    del dataurl
    # Return, with simple check
    if data is None:
        print("No data could be fetched, exiting...")
        sys.exit()
    else:
        return (data, data_name)
def arg_parsing_datapath(_, datapath) -> Tuple[pandas.DataFrame, str]:
    """
    When a datapath was provided, reads file and returns data
    """
    del _
    print("Filepath passed as data, reading file...")
    # Loop-try on different formats
    for option in [
        pandas.read_csv,
        pandas.read_json
        ]:
        try:
            data = option(datapath)
            data_name = re.split(r"/|\\", datapath)[-1]
            break #if format tried is valid, break loop to go to return
        except ValueError:
            pass
    del option
    del datapath
    # Return, with simple check
    if data is None:
        print("No data could be read, exiting...")
        sys.exit()
    else:
        return (data, data_name)