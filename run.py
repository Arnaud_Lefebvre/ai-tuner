"""
TOCOMPLETE - This module returns the most appropriate machine learning model
    with the most appropriate set of parameters for it to be most efficient
    for a given dataset
    > needn't say, within its own limitations
It loads any try that it does in a database so as to be able to reuse the information

TOCOMPLETE - Options
    model choice
    parameters choice

TODO LIST
var integrity check fill
dataset name, line count, ?? table 
add column check extend features

add column concatenation to predict multiple columns
variable save of associations&Co to skip requestting db whenever possible
indices to fasten database requesting/updating
add dataset name (or auto create frrom source..?)
add var deletes
add ask on case database not setup properly, database backup Y/N && continue Y/N
make everything possible a variable, for stability and option giving
config.cfg to change parameters & feed data without using option
parameters LOAD outside any unnecessary loop
add NaN handling
add measure and save variance to default values
history of execution times
    -> prompt user "this is expeected to take __min__sec, continue ?"
more models
more parameters
more data formats
other sources of ai
recursive Y/N ; process time limit ; AI on results Y/N
doc recommended / not recommended
doc
data analysis and automatic recommendations
save results Y/N
automatically write code to use ai
dash report Y/N
column selection
test_size testing
results repartition over time
import ai libraries choice
"""
# Built-in imports
from collections import defaultdict
import datetime
import inspect
from scripts.arg_parsing_handling.arg_parsing_main import arg_parsing_main
import sqlite3
import time
from typing import Dict, Iterable, List
# External librairies imports
import pandas
from sklearn import preprocessing
from sklearn.dummy import DummyClassifier
from sklearn.model_selection import train_test_split
from sklearn import tree
# Custom librairies imports
from scripts import var_integrity_check
from scripts.arg_parsing_handling import arg_parsing_main
from scripts.core import do_column_check
from scripts.database_handling import database_main
from scripts.miscellaneous import execution_time_decorator
from scripts.core import param_lists_to_combinations_list, setup_joints
from scripts.sklearn_scripts import get_dummy_high_score


# WIP
# Variables
#
#
# Variables default values
#
# Generic
database_filepath = "records.db"
column_check = None
mode = None #0, 1, 2
data = None
data_name = None
data_line_count = None
test_train_splits = [0.3] # 0.1 -> 0.9 (step 0.1 not activated)
# data_X_columns = ["Sepal.Length", "Sepal.Width", "Petal.Length", "Petal.Width"]
# data_y_column = "Species"
data_X_columns = [
    "tem_age", "tem_genre", "tem_xp_activite_type", "obs_date_heure", "obs_1_lat", "obs_1_lon", "obs_1_env_sol_type",
    "heure", "jour", "mois", "jour_semaine"
]
data_y_column = "cas_classification"
overall_repeats = 10
random_split_repeats = 1 #Not implemented properly
dummy_score_repeats = 1
column_check_repeats = 1
param_check_repeats = 1
model_dict = {
    "sklearn": [
        DummyClassifier(),
        tree.DecisionTreeClassifier()
    ]
}
dummy_strategies = {
    "sklearn": ["stratified", "most_frequent", "prior", "uniform", "constant"]
}

# scikit learn
# DecisionTreeClassifier
#   default parameters are set for maximum accuracy - i think
ccp_alpha_list = [0.0]
class_weight_list = [None]
criterion_list = ["gini", "entropy"] # "gini" | "entropy"
max_depth_list = [None]
max_features_list = [None]
max_leaf_nodes_list = [None]
min_impurity_decrease_list = [0.0]
min_impurity_split_list = [None]
min_samples_leaf_list = [1]
min_samples_split_list = [2]
min_weight_fraction_leaf_list = [0.0]
random_state_list = [None]
splitter_list = ["best", "random"] # "best" | "random"

parameters_default = [
    0.0,
    None,
    "gini",
    None,
    None,
    None,
    0.0,
    None,
    1,
    2,
    0.0,
    None,
    "best"
]

parameters_dict = {
    "sklearn": {
        str(DummyClassifier())[:-2]: {
            "strategy": ["stratified", "most_frequent", "prior", "uniform", "constant"]
        },
        str(tree.DecisionTreeClassifier())[:-2]: {
            "ccp_alpha_list": ccp_alpha_list,
            "class_weight_list": class_weight_list,
            "criterion_list": criterion_list,
            "max_depth_list": max_depth_list,
            "max_features_list": max_features_list,
            "max_leaf_nodes_list": max_leaf_nodes_list,
            "min_impurity_decrease_list": min_impurity_decrease_list,
            "min_impurity_split_list": min_impurity_split_list,
            "min_samples_leaf_list": min_samples_leaf_list,
            "min_samples_split_list": min_samples_split_list,
            "min_weight_fraction_leaf_list": min_weight_fraction_leaf_list,
            "random_state_list": random_state_list,
            "splitter_list": splitter_list
        }
    }
}

# Functional variables
start_time = time.time()

#

# Variables default values


# Functions
#
#
# Functions - auxiliary
#
# Variables import from cfg.py
# WORK IN PROGRESS - MAY BE DELETED
def variables_import():
    """
    Imports variables from file cfg.py
    """
    supported_models_list = []
    for l in model_dict.values():
        supported_models_list.extend(l)
    cfg_vars_dict = dict(globals().get("cfg").__dict__)
    for k,v in {kk:vv for kk,vv in cfg_vars_dict.items() if not kk.startswith("__")}.items():
        t = type(v)
        if v is None:
            globals()[k] = None
        elif isinstance(v, (str, int, list, tuple, dict, float, bool, *list(map(type, supported_models_list)))):
            globals()[k] = t(v)
# Execution time handling
def get_time_start() -> float:
    return time.time()
def get_time_spent_ms(exec_start_time:float) -> float:
    return (time.time() - exec_start_time)*1000

# Functions - core subfunctions
#
# run_record_ai_tests() sub-functions

def tune_sklearn_decision_tree_classifier(
    ccp_alpha,
    class_weight,
    criterion,
    max_depth,
    max_features,
    max_leaf_nodes,
    min_impurity_decrease,
    min_impurity_split,
    min_samples_leaf,
    min_samples_split,
    min_weight_fraction_leaf,
    random_state,
    splitter,
    connection: sqlite3.Connection,
    cursor: sqlite3.Cursor,
    X_train: pandas.DataFrame,
    X_test: pandas.Series,
    y_train: pandas.DataFrame,
    y_test: pandas.Series,
    serie_id: int,
    model_param_group_id: int,
    dataset_column_group_id: int
    ):
    """
    
    """
    time_start = get_time_start()
    tree_clf = tree.DecisionTreeClassifier(
        ccp_alpha=ccp_alpha,
        class_weight=class_weight,
        criterion=criterion,
        max_depth=max_depth,
        max_features=max_features,
        max_leaf_nodes=max_leaf_nodes,
        min_impurity_decrease=min_impurity_decrease,
        min_impurity_split=min_impurity_split,
        min_samples_leaf=min_samples_leaf,
        min_samples_split=min_samples_split,
        min_weight_fraction_leaf=min_weight_fraction_leaf,
        random_state=random_state,
        splitter=splitter
    )
    # for col in ["cas_classification", "tem_genre", "tem_xp_activite_type", "obs_1_env_sol_type"]:
    #     encoder_dict = defaultdict(preprocessing.LabelEncoder())
    #     data = data.apply(lambda x: encoder_dict[x.name].fit_transform(x))
    # for col in X_train.columns:
    #     le = preprocessing.LabelEncoder()
    #     le.fit(X_train[col])
    #     X_train[col] = X_train[col].apply(lambda x: le.fit_transform(x))
    # for col in y_train.columns:
    #     le = preprocessing.LabelEncoder()
    #     le.fit(y_train[col])
    #     y_train[col] = y_train[col].apply(lambda x: le.fit_transform(x))
    tree_clf.fit(X_train, y_train)
    score = tree_clf.score(X_test, y_test)
    execution_time = get_time_spent_ms(time_start)
    cursor.execute(
        f"""
        INSERT INTO main
            (
                score,
                execution_time_ms,
                model_param_group_id,
                dataset_column_group_id,
                serie_id
            )
        VALUES
            (
                {score},
                {execution_time},
                {model_param_group_id},
                {dataset_column_group_id},
                {serie_id}
            )
        """
    )
    # connection.commit()




    # ccp_alpha_list = [0.0]
    # class_weight_list = [None]
    # criterion_list = ["gini", "entropy"] # "gini" | "entropy"
    # max_depth_list = [None]
    # max_features_list = [None]
    # max_leaf_nodes_list = [None]
    # min_impurity_decrease_list = [0.0]
    # min_impurity_split_list = [None]
    # min_samples_leaf_list = [1]
    # min_samples_split_list = [2]
    # min_weight_fraction_leaf_list = [0.0]
    # random_state_list = [None]
    # splitter_list = ["best", "random"] # "best" | "random"
    # Loop on all parameter combinations
    # for ccp_alpha in ccp_alpha_list:
    #     for class_weight in class_weight_list:
    #         for criterion in criterion_list:
    #             for max_depth in max_depth_list:
    #                 for max_features in max_features_list:
    #                     for max_leaf_nodes in max_leaf_nodes_list:
    #                         for min_impurity_decrease in min_impurity_decrease_list:
    #                             for min_impurity_split in min_impurity_split_list:
    #                                 for min_samples_leaf in min_samples_leaf_list:
    #                                     for min_samples_split in min_samples_split_list:
    #                                         for min_weight_fraction_leaf in min_weight_fraction_leaf_list:
    #                                             for random_state in random_state_list:
    #                                                 for splitter in splitter_list:
    #                                                     # Run a tree with these parameters
    #                                                     time_start = get_time_start()
    #                                                     tree_clf = tree.DecisionTreeClassifier(
    #                                                         ccp_alpha=ccp_alpha,
    #                                                         class_weight=class_weight,
    #                                                         criterion=criterion,
    #                                                         max_depth=max_depth,
    #                                                         max_features=max_features,
    #                                                         max_leaf_nodes=max_leaf_nodes,
    #                                                         min_impurity_decrease=min_impurity_decrease,
    #                                                         min_impurity_split=min_impurity_split,
    #                                                         min_samples_leaf=min_samples_leaf,
    #                                                         min_samples_split=min_samples_split,
    #                                                         min_weight_fraction_leaf=min_weight_fraction_leaf,
    #                                                         random_state=random_state,
    #                                                         splitter=splitter
    #                                                     )
    #                                                     tree_clf.fit(X_train, y_train)
    #                                                     score = tree_clf.score(X_test, y_test)
    #                                                     execution_time = get_time_spent_ms(time_start)

                                                        # # Parameters load and group_id fetch
                                                        # param_id_list = []
                                                        # # Add any missing parameter-value combination and fetch combination id
                                                        # for param_key, param_value in tree_clf.get_params().items():
                                                        #     cursor.execute(
                                                        #         f"""
                                                        #         INSERT OR IGNORE INTO param_ref(name, value)
                                                        #         VALUES ('{param_key}', '{param_value}')
                                                        #         """
                                                        #     )
                                                        #     cursor.execute(
                                                        #         f"""
                                                        #         SELECT id
                                                        #         FROM param_ref
                                                        #         WHERE name='{param_key}'
                                                        #             AND value='{param_value}'
                                                        #         """
                                                        #     )
                                                        #     param_id = cursor.fetchone()[0]
                                                        #     param_id_list.append(param_id)
                                                        # param_count = len(param_id_list)
                                                        # # Turn ids list in a string for SQL use
                                                        # param_id_list_concatenated = ""
                                                        # for id in param_id_list:
                                                        #     param_id_list_concatenated += str(id) + ","
                                                        # param_id_list_concatenated = param_id_list_concatenated[:-1] #remove trailing comma
                                                        # # Fetch group id for current parameters set if it exists
                                                        # cursor.execute(
                                                        #     f"""
                                                        #     SELECT param_group_id
                                                        #     FROM param_assoc
                                                        #     WHERE param_group_id
                                                        #         IN (
                                                        #             SELECT param_group_id
                                                        #             FROM param_assoc
                                                        #             WHERE param_id IN ({param_id_list_concatenated})
                                                        #             GROUP BY param_group_id
                                                        #             HAVING count(*) = {param_count}
                                                        #         )
                                                        #     GROUP BY param_group_id
                                                        #     HAVING count(*) = {param_count}
                                                        #     """
                                                        # )
                                                        # param_group_id = cursor.fetchone()
                                                        # if param_group_id is not None: #if group id exists, use it
                                                        #     param_group_id = param_group_id[0]
                                                        # else: #else group id does not exist, create it
                                                        #     cursor.execute(
                                                        #         """
                                                        #         INSERT INTO param_group_ref DEFAULT VALUES
                                                        #         """
                                                        #     )
                                                        #     param_group_id = cursor.lastrowid
                                                        #     # And create relationship in param_assoc table
                                                        #     for id in param_id_list:
                                                        #         cursor.execute(
                                                        #             f"""
                                                        #             INSERT INTO param_assoc(param_id, param_group_id)
                                                        #             VALUES ({id}, {param_group_id})
                                                        #             """
                                                        #         )
                                            
                                                        # # Record results
                                                        # model_source = "sklearn"
                                                        # model = "DecisionTreeClassifier"
                                                        # current_time = datetime.datetime.now()
                                                        # cursor.execute(
                                                        #     f"""
                                                        #     INSERT INTO main(
                                                        #         score,
                                                        #         execution_time_ms,
                                                        #         model_source,
                                                        #         model,
                                                        #         dummy_id,
                                                        #         param_group_id,
                                                        #         test_train_split,
                                                        #         train_line_count,
                                                        #         test_line_count,
                                                        #         serie_id,
                                                        #         datetime 
                                                        #     )
                                                        #     VALUES (
                                                        #             {score},
                                                        #             {execution_time},
                                                        #             '{model_source}',
                                                        #             '{model}',
                                                        #             {dummy_id},
                                                        #             {param_group_id},
                                                        #             {test_train_split},
                                                        #             {train_line_count},
                                                        #             {test_line_count},
                                                        #             {serie_id},
                                                        #             '{current_time}'
                                                        #         )
                                                        #     """
                                                        # )

# Functions - core
#
# @execution_time_decorator.execution_time_decorator("running tests", start_time, inspect.getframeinfo(inspect.currentframe()).lineno)
def run_record_ai_tests(
    connection: sqlite3.Connection,
    cursor: sqlite3.Cursor,
    ids_reference_dict_dict,
    data: pandas.DataFrame,
    column_combinations_list: Iterable[str],
    data_X_columns: Iterable[str],
    data_y_column: str,
    model_dict: Iterable[object],
    parameters_dict,
    dummy_strategies: Iterable[List],
    data_name: str,
    data_line_count: int,
    test_train_splits: Iterable[float],
    overall_repeats: int,
    random_split_repeats: int,
    dummy_score_repeats: int,
    column_check_repeats: int,
    param_check_repeats: int,
    parameters_default: Iterable
    ) -> None:
    serie_datetime = str(datetime.datetime.now())
    cursor.execute(
        f"""
        INSERT INTO serie_ref(
            datetime_at_runtime_start, mode
        )
        VALUES
            ('{serie_datetime}', 'dummy')
        """
    )
    serie_dummy_id = cursor.lastrowid
    cursor.execute(
        f"""
        INSERT INTO serie_ref(
            datetime_at_runtime_start, mode
        )
        VALUES
            ('{serie_datetime}', 'column')
        """
    )
    serie_column_id = cursor.lastrowid
    cursor.execute(
        f"""
        INSERT INTO serie_ref(
            datetime_at_runtime_start, mode
        )
        VALUES
            ('{serie_datetime}', 'param')
        """
    )
    serie_param_id = cursor.lastrowid
    X = pandas.DataFrame(data.drop(data_y_column, axis=1))
    y = pandas.Series(data[data_y_column])
    ss_time = time.time()
    for overall_repeat in range(overall_repeats):
        current_time_from_start = time.time() - ss_time
        print(f"Starting overall repeat n°{overall_repeat+1} at time mark: {current_time_from_start}")
        for random_split_repeat in range(1,random_split_repeats+1):
            for test_train_split in test_train_splits:
                X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_train_split, random_state=random_split_repeat)
                # train_line_count = len(y_train)
                # test_line_count = len(y_test)


                loop_highest_score, loop_highest_score_strategy, dummy_execution_time = get_dummy_high_score.get_dummy_high_score(
                    X_train=X_train,
                    X_test=X_test,
                    y_train=y_train,
                    y_test=y_test,
                    dummy_scores_repeats=dummy_score_repeats,
                    dummy_strategies=dummy_strategies["sklearn"]
                )
                dummy_model_param_group_id = ids_reference_dict_dict[("sklearn", str(DummyClassifier())[:-2])][(loop_highest_score_strategy,)]
                dummy_dataset_column_group_id = ids_reference_dict_dict[(data_name, data_line_count, test_train_split)][tuple(data_X_columns)]
                cursor.execute(
                    f"""
                    INSERT INTO main (
                        score,
                        execution_time_ms,
                        model_param_group_id,
                        dataset_column_group_id,
                        serie_id
                        )
                    VALUES
                        (
                        {loop_highest_score},
                        {dummy_execution_time},
                        {dummy_model_param_group_id},
                        {dummy_dataset_column_group_id},
                        {serie_dummy_id}
                        )
                    """
                )
                # call tune_sklearn_decision_tree_classifier on columns
                for column_combination in column_combinations_list:
                    column_combination_as_list = list(column_combination)
                    for _ in range(column_check_repeats):
                        model_param_group_id = \
                            ids_reference_dict_dict[("sklearn", str(tree.DecisionTreeClassifier())[:-2])][tuple(parameters_default)]
                        dataset_column_group_id = \
                            ids_reference_dict_dict[(data_name, data_line_count, test_train_split)][tuple(column_combination)]
                        tune_sklearn_decision_tree_classifier(
                            *parameters_default,
                            connection=connection,
                            cursor=cursor,
                            X_train=X_train[column_combination_as_list],
                            X_test=X_test[column_combination_as_list],
                            y_train=y_train,
                            y_test=y_test,
                            serie_id=serie_column_id,
                            model_param_group_id=model_param_group_id,
                            dataset_column_group_id=dataset_column_group_id
                        )
                parameter_combinations = param_lists_to_combinations_list.param_lists_to_combinations_list(
                    list(parameters_dict["sklearn"][str(tree.DecisionTreeClassifier())[:-2]].values())
                )
                # call tune_sklearn_decision_tree_classifier on params
                # for parameter_combination in list(parameters_dict["sklearn"][str(tree.DecisionTreeClassifier())[:-2]].values()):
                for parameter_combination in parameter_combinations:
                    for _ in range(param_check_repeats):
                        model_param_group_id = \
                            ids_reference_dict_dict[("sklearn", str(tree.DecisionTreeClassifier())[:-2])][tuple(parameter_combination)]
                        dataset_column_group_id = \
                            ids_reference_dict_dict[(data_name, data_line_count, test_train_split)][tuple(data_X_columns)]
                        tune_sklearn_decision_tree_classifier(
                            *parameter_combination,
                            connection=connection,
                            cursor=cursor,
                            X_train=X_train,
                            X_test=X_test,
                            y_train=y_train,
                            y_test=y_test,
                            serie_id=serie_param_id,
                            model_param_group_id=model_param_group_id,
                            dataset_column_group_id=dataset_column_group_id
                        )
        connection.commit()
    return None

# Exec
if __name__ == "__main__":
    # data = pandas.read_csv("iris.csv")
    # data = pandas.read_csv(io.StringIO(
    #   "https://gist.githubusercontent.com/curran/a08a1080b88344b0c8a7/raw/0e7a9b0a5d22642a06d3d5b9bcbad9890c8ee534/iris.csv"))

    entire_time_start = get_time_start() #to remove ultimately

    # var_integrity_check.check_test_sizes(test_sizes) #only one var - useless atm

    data, data_name, column_check = arg_parsing_main.arg_parsing_main()

    # GEIPAN CASE
    #
    #
    import re
    unique_tem_age = data["tem_age"].unique()
    tem_age_map = {}
    recomp = re.compile(r"\d+")
    for un in unique_tem_age:
        new = re.search(recomp, un).group()
        tem_age_map[un] = new
    data = data.replace({"tem_age": tem_age_map})
    data["tem_age"] = data["tem_age"].astype("int")

    data["obs_date_heure"] = pandas.to_datetime(data["obs_date_heure"])

    data["heure"] = pandas.Series(data["obs_date_heure"].dt.hour)
    data["jour"] = pandas.Series(data["obs_date_heure"].dt.day)
    data["mois"] = pandas.Series(data["obs_date_heure"].dt.month)
    data["jour_semaine"] = pandas.Series(data["obs_date_heure"].dt.dayofweek)
    le = preprocessing.LabelEncoder()
    for col in data.columns:
        le.fit(data[col])
    data = data.apply(lambda x: le.fit_transform(x))
    #
    #
    #

    data_line_count = len(data)
    connection, cursor = database_main.database_main(database_filepath=database_filepath)
    column_combinations_list = do_column_check.do_column_check(
        mode=2,
        data=data,
        data_X_columns=data_X_columns,
        data_y_column=data_y_column
    )
    ids_reference_dict_dict = setup_joints.do_all_join_insert(
        connection=connection,
        cursor=cursor,
        data_name=data_name,
        data_line_count=data_line_count,
        test_train_splits=test_train_splits,
        column_combination_list=column_combinations_list,
        model_dict=model_dict,
        param_dict=parameters_dict
    )

    run_record_ai_tests(
        connection=connection,
        cursor=cursor,
        ids_reference_dict_dict=ids_reference_dict_dict,
        data=data,
        column_combinations_list=column_combinations_list,
        data_X_columns=data_X_columns,
        data_y_column=data_y_column,
        model_dict=model_dict,
        parameters_dict=parameters_dict,
        dummy_strategies=dummy_strategies,
        data_name=data_name,
        data_line_count=data_line_count,
        test_train_splits=test_train_splits,
        overall_repeats=overall_repeats,
        random_split_repeats=random_split_repeats,
        dummy_score_repeats=dummy_score_repeats,
        column_check_repeats=column_check_repeats,
        param_check_repeats=param_check_repeats,
        parameters_default=parameters_default
    )

    cursor.close()
    connection.close()
    entire_time_spent = (datetime.datetime.now().timestamp() - entire_time_start) #to remove ultimately
    print("Total execution time:", entire_time_spent) #to remove ultimately