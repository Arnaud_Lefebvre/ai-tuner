"""

"""
#

import pandas
import pytest

#


from run import get_time_start
from run import get_time_spent


def test_get_time_start():
    def test_returns_float():
        assert isinstance(get_time_start(), float)
    def test_returns_valid_value():
        assert get_time_start() > 1_500_000_000
    test_returns_float()
    test_returns_valid_value()

def test_get_time_spent():
    def test_returns_float():
        assert isinstance(get_time_spent(0.0), float)
        assert isinstance(get_time_spent(1000000000), float)
        assert isinstance(get_time_spent(-1000000000), float)
    test_returns_float()