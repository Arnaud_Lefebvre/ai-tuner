# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import sqlite3

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# dataset_name = "iris.csv"
dataset_name = "geipan.csv"
connection = sqlite3.connect("records.db")
FROM_WHERE_query_part = f"""
    FROM
            main
        JOIN
            serie_ref ON serie_ref.id = main.serie_id
        JOIN
            dataset_column_group_assoc ON dataset_column_group_assoc.id = main.dataset_column_group_id
        JOIN
            dataset_ref ON dataset_ref.id = dataset_column_group_assoc.dataset_id
        JOIN
            column_group_ref ON column_group_ref.id = dataset_column_group_assoc.column_group_id
        JOIN
            column_assoc ON column_assoc.column_group_id = column_group_ref.id
        JOIN
            column_ref ON column_ref.id = column_assoc.column_id
    WHERE
            (serie_ref.mode = 'column')
    AND
        (dataset_ref.name = '{dataset_name}')
"""
# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
cursor = connection.cursor()
cursor.execute(
    f"""
    SELECT
            COUNT(*)
    {FROM_WHERE_query_part}
    """
)
line_count = cursor.fetchall()[0][0]
df = pandas.read_sql(
    sql=f"""
        SELECT
            main.score,
            column_group_ref.id AS columns
        {FROM_WHERE_query_part}
        ORDER BY
            main.score DESC
    ;""",
    con=connection
)
cursor.execute(
    f"""
    SELECT
        column_group_ref.id
    {FROM_WHERE_query_part}
    GROUP BY
        column_group_ref.id
    """
)
column_group_ids_list = [x[0] for x in cursor.fetchall()]
names_map = {}
for id in column_group_ids_list:
    cursor.execute(
        f"""
        SELECT
            column_ref.name
        {FROM_WHERE_query_part}
        AND
            column_group_ref.id = {str(id)}
        GROUP BY
            column_group_ref.id,
            column_ref.name
        """
    )
    column_names = [x[0] for x in cursor.fetchall()]
    column_names_as_string = ""
    for column_name in column_names:
        column_names_as_string += column_name+"<br>"
    column_names_as_string = column_names_as_string[:-4]
    names_map[id] = column_names_as_string

cursor.close()
connection.close()

df = df.replace({"columns": names_map})



fig = px.box(
    df,
    x="columns",
    y="score",
    range_y=[0,1]
)

app.layout = html.Div(children=[
    html.H1(children=f'Columns benchmark for <{dataset_name}>'),

    html.Div(children=f'''
        Reporting scores obtained over {line_count} records in database:
    '''),

    dcc.Graph(
        id='column-graph',
        figure=fig
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)