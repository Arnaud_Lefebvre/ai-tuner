# Functions documentation

## Execution time functions

#### <div id="get_time_start">get_time_start()</div>

a

#### <div id="get_time_spent">get_time_spent()</div>

a

## Setup functions

### <div id="arg_parsing">arg_parsing()</div>

a

#### <div id="arg_parsing_case_switcher">arg_parsing_case_switcher()</div>

a

#### <div id="arg_parsing_action_switcher">arg_parsing_action_switcher()</div>

a

#### <div id="arg_parsing_no_args">arg_parsing_no_args()</div>

a

#### <div id="arg_parsing_multiple_args">arg_parsing_multiple_args()</div>

a

#### <div id="arg_parsing_dataurl">arg_parsing_dataurl()</div>

a

#### <div id="arg_parsing_datapath">arg_parsing_datapath()</div>

a

### <div id="database_setup">database_setup()</div>

a

#### <div id="database_check_existence">database_check_existence()</div>

a

#### <div id="database_get_connection_and_cursor">database_get_connection_and_cursor()</div>

a

#### <div id="database_activate_foreign_keys">database_activate_foreign_keys()</div>

a

#### <div id="database_tables_setup">database_tables_setup()</div>

a

## Runtime functions

### <div id="run_record_ai_tests">run_record_ai_tests()</div>

a

#### <div id="get_dummy_high_score">get_dummy_high_score()</div>

a

#### <div id="tune_sklearn_decision_tree_classifier">tune_sklearn_decision_tree_classifier()</div>

a