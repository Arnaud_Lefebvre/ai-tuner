"""
Container for globals variables
"""
from sklearn import tree
# Global variables
dummy_strategies = ["stratified", "most_frequent", "prior", "uniform", "constant"] # list of string(s)
    # DEFAULT dummy_strategies = ["stratified", "most_frequent", "prior", "uniform", "constant"]
data = None
data_X_columns = None
data_y_column = None
model_list = [tree.DecisionTreeClassifier()]
    # list of one or more of tree.DecisionTreeClassifier()
serie_id = None # any - recommended: int or None
    # DEFAULT serie_id = None
test_size = 0.3 # 0.0 < float < 1.0
    # DEFAULT test_size = 0.3
dummy_scores_repeats = 1 # int
    # DEFAULT dummy_scores_repeats = 10
simple_repeats = 1 # int
    # DEFAULT simple_repeats = 10
split_repeats = 1 # int
    # DEFAULT split_repeats = 10
