```mermaid
classDiagram

class main
<<FACTS>> main
class main {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    score REAL_NOT_NULL
    execution_time_ms REAL_NOT_NULL
    model_id INTEGER_NOT_NULL
    dummy_id INTEGER_NOT_NULL
    param_group_id INTEGER_NOT_NULL
    dataset_id INTEGER NOT NULL
    serie_id INTEGER_NOT_NULL
    FOREIGN_KEY_dummy_id_REFERENCES_dummy_ref(id)
    FOREIGN_KEY_param_group_id_REFERENCES_dummy_ref(id)
    FOREIGN_KEY_dataset_id_REFERENCES_dataset_ref(id)
    CHECK(1>=score)
    CHECK(score>=0)
}
class model_ref
<<REFERENCE>> model_ref
class model_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    source TEXT_NOT_NULL
    name TEXT_NOT_NULL
    UNIQUE(source, name)
}
class serie_ref
<<REFERENCE>> serie_ref
class serie_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    datetime TEXT_NOT_NULL
}
class dummy_ref
<<REFERENCE>> dummy_ref
class dummy_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    dummy_score REAL_NOT_NULL
    dummy_repeats INTEGER_NOT_NULL
    UNIQUE(dummy_score, dummy_repeats)
    CHECK(1>=dummy_score)
    CHECK(dummy_score>=0)
}
class dataset_ref
<<REFERENCE>> dataset_ref
class dataset_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    dataset_name TEXT
    line_count INTEGER_NOT_NULL
    test_train_split REAL_NOT_NULL
    UNIQUE(dataset_name, line_count, test_train_split)
    CHECK(0.9>=test_train_split)
    CHECK(test_train_split>=0.1)
}
class column_ref
<<REFERENCE>> column_ref
class column_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    column_name TEXT_NOT_NULL
    score REAL_NOT_NULL
    CHECK(1>=score)
    CHECK(score>=0)
}
class column_assoc
<<ASSOCIATION>> column_assoc
class column_assoc {
    column_id INTEGER_NOT_NULL
    dataset_id INTEGER_NOT_NULL
    PRIMARY_KEY(column_id, score_id)
    FOREIGN_KEY_column_id_REFERENCES_column_ref(id)
    FOREIGN_KEY_dataset_id_REFERENCES_dataset_ref(id)
}
class param_ref
<<REFERENCE>> param_ref
class param_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
    name TEXT_NOT_NULL
    value TEXT_NOT_NULL
    UNIQUE(name, value)
}
class param_group_ref
<<REFERENCE>> param_group_ref
class param_group_ref {
    id INTEGER_NOT_NULL_PRIMARY_KEY_AUTOINCREMENT
}
class param_assoc
<<ASSOCIATION>> param_assoc
class param_assoc {
    param_id INTEGER_NOT_NULL
    param_group_id INTERGER_NOT_NULL
    PRIMARY_KEY(param_id, param_group_id)
    FOREIGN_KEY_param_id_REFERENCES_param_ref(id)
    FOREIGN_KEY_param_group_id_REFERENCES_param_group_ref(id)
}

main "0,*" --|> "1,1" model_ref: main.model_id REFERS TO model_ref.id
main "0,*" --|> "1,1" serie_ref: main.serie_id REFERS TO serie_ref.id
main "0,*" --|> "1,1" dummy_ref: main.dummy_id REFERS TO dummy_ref.id
main "0,*" --|> "1,1" param_group_ref: main.param_group_id REFERS TO param_group_ref.id
param_assoc "0,*" --|> "1,1" param_ref: param_assoc.param_id REFERS TO param_ref.id
param_assoc "0,*" --|> "1,1" param_group_ref: param_assoc.param_group_id REFERS TO param_group_ref.id
main "0,*" --|> "1,1" dataset_ref: main.dataset_id REFERS TO dataset_ref.id
column_assoc "0,*" --|> "1,1" dataset_ref: column_assoc.dataset_id REFERS TO dataset_ref.id
column_assoc "0,*" --|> "1,1" column_ref: column_assoc.column_id REFERS TO column_ref.id
```