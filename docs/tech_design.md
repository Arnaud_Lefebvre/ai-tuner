# Functions flowchart

> Many functions can be clicked to reach them in the documentation

```mermaid
flowchart TB

run("run.py")

subgraph ARG_PARSING
direction TB
arg("arg_parsing()")
argindexcase("arg_parsing_case_switcher()")
argactioncase("arg_parsing_action_switcher()")
argno("arg_parsing_no_args()")
argmult("arg_parsing_multiple_args()")
argurl("arg_parsing_dataurl()")
argpath("arg_parsing_datapath()")
arg -- "switch action index" --> argindexcase
argindexcase -- "switch action" --> argactioncase
argactioncase -. "do switched action" .-> argno
argactioncase -. "do switched action" .-> argmult
argactioncase -. "do switched action" .-> argurl
argactioncase -. "do switched action" .-> argpath
argno -. "return data" .-> arg
argmult -. "return data" .-> arg
argurl -. "return data" .-> arg
argpath -. "return data" .-> arg
end

subgraph DATABASE_SETUP
direction TB
db("database_setup()")
dbex("database_check_existence()")
dbcon("database_get_connection_and_cursor()")
dbfk("database_activate_foreign_keys()")
dbta("database_tables_setup()")
db -- "Create database if needed" --> dbex
db -- "Call" --> dbcon
dbcon -- "Returns connection" --> db
db -- "Activates sqlite3 foreign keys" --> dbfk
db -- "Create tables if needed" --> dbta
end

subgraph RUN_RECORD_AI_TESTS
direction TB
runt("run_record_ai_tests()")
TODOfetch_create_group_id
TODOrun_model
TODOload_results
runt --> TODOfetch_create_group_id
TODOfetch_create_group_id --> TODOrun_model
TODOrun_model --> TODOload_results
end

subgraph EXECUTION_TIME_FUNCTIONS
tst("get_time_start()")
tsp("get_time_spent()")
end

TODOfetch_create_group_id -.-> tst
tst -.-> TODOrun_model
TODOrun_model -.-> tsp
tsp -.-> TODOload_results


run -- 1111 --> ARG_PARSING
ARG_PARSING -- 2222 --> DATABASE_SETUP
DATABASE_SETUP -- 3333 --> RUN_RECORD_AI_TESTS

click arg "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing"
click argindexcase "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing_case_switcher"
click argactioncase "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing_action_switcher"
click argno "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing_no_args"
click argmult "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing_multiple_args"
click argurl "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing_dataurl"
click argpath "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#arg_parsing_datapath"

click db "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#database_setup"
click dbex "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#database_check_existence"
click dbcon "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#database_get_connection_and_cursor"
click dbfk "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#database_activate_foreign_keys"
click dbta "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#database_tables_setup"

click runt "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#run_record_ai_tests"

click tst "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#get_time_start"
click tsp "https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/doc.md/#get_time_spent"
```