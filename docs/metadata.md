# Metadata

## Columns - non-ID

### main

#### score

The score of the machine learning model iteration.

#### execution_time_ms

The time taken to obtain a score of the machine learning model iteration.

### serie_ref

#### datetime_at_runtime_start

The near iso format 8601 datetime around program execution start.

#### mode

The focus of the program for the related records.  
Can be one of:
- "dummy": a simple, random model to get a reference minimal score.
- "column": a focus on benchmarking the dataset's columns using default model & parameters.
- "param": a focus on benchmarking the models and their parameters using default or most appropriate columns.

### dataset_ref

#### name

The dataset's name.

#### line_count

The dataset's line count.

#### test_train_split

When splitting the dataset in two to be able to first train the model and then test it,  
this is the dataset's ratio of lines between testing and training.  
e.g.: 100 lines and ratio 0.3 - 30 testing lines and 70 training lines.

### column_ref

#### name

The column name.

#### data_type

The column data type.
> only non required value at the moment

### model_ref

#### source

The model's source.

#### name

The model's name.

### param_ref

#### name

The parameter's name.

#### value

The parameter's value.

## Columns - IDs

### main

#### id

The standard primary key - no column or combination of column could be one

#### model_param_group_id

The foreign key referencing table ```model_param_group_assoc```

#### dataset_column_group_id

The foreign key referencing table ```dataset_column_group_assoc```

#### serie_id

The foreign key referencing table ```serie_ref```

### serie_ref

#### id

### dataset_ref

#### id

### dataset_column_group_assoc

#### id

#### dataset_id

#### column_group_id

### column_group_ref

#### id

### column_assoc

#### column_group_id

#### column_id

### column_ref

#### id

### model_ref

#### id

### model_param_group_assoc

#### id

#### model_id

#### param_group_id

### param_group_ref

#### id

### param_assoc

#### param_group_id

#### param_id

### param_ref

#### id