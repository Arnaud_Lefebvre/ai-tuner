-- Every possible JOIN
SELECT
    *
FROM
        main
    JOIN
        serie_ref ON serie_ref.id = main.serie_id
    JOIN
        dataset_column_group_assoc ON dataset_column_group_assoc.id = main.dataset_column_group_id
    JOIN
        dataset_ref ON dataset_ref.id = dataset_column_group_assoc.dataset_id
    JOIN
        column_group_ref ON column_group_ref.id = dataset_column_group_assoc.column_group_id
    JOIN
        column_assoc ON column_assoc.column_group_id = column_group_ref.id
    JOIN
        column_ref ON column_ref.id = column_assoc.column_id
    JOIN
        model_param_group_assoc ON model_param_group_assoc.id = main.model_param_group_id
    JOIN
        model_ref ON model_ref.id = model_param_group_assoc.model_id
    JOIN
        param_group_ref ON param_group_ref.id = model_param_group_assoc.param_group_id
    JOIN
        param_assoc ON param_assoc.param_group_id = param_group_ref.id
    JOIN
        param_ref ON param_ref.id = param_assoc.param_id
;
-- SELECT every non-ID column
SELECT
    main.score,
    main.execution_time_ms,
    serie_ref.datetime_at_runtime_start,
    serie_ref.mode,
    dataset_ref.name,
    dataset_ref.line_count,
    dataset_ref.test_train_split,
    column_ref.name,
    column_ref.data_type,
    model_ref.source,
    model_ref.name,
    param_ref.name,
    param_ref.value
FROM
        main
    JOIN
        serie_ref ON serie_ref.id = main.serie_id
    JOIN
        dataset_column_group_assoc ON dataset_column_group_assoc.id = main.dataset_column_group_id
    JOIN
        dataset_ref ON dataset_ref.id = dataset_column_group_assoc.dataset_id
    JOIN
        column_group_ref ON column_group_ref.id = dataset_column_group_assoc.column_group_id
    JOIN
        column_assoc ON column_assoc.column_group_id = column_group_ref.id
    JOIN
        column_ref ON column_ref.id = column_assoc.column_id
    JOIN
        model_param_group_assoc ON model_param_group_assoc.id = main.model_param_group_id
    JOIN
        model_ref ON model_ref.id = model_param_group_assoc.model_id
    JOIN
        param_group_ref ON param_group_ref.id = model_param_group_assoc.param_group_id
    JOIN
        param_assoc ON param_assoc.param_group_id = param_group_ref.id
    JOIN
        param_ref ON param_ref.id = param_assoc.param_id
;
-- COUNT line total in every table
SELECT
    COUNT(*)
FROM
    main
;
SELECT
    COUNT(*)
FROM
    serie_ref
;
SELECT
    COUNT(*)
FROM
    dataset_column_group_assoc
;
SELECT
    COUNT(*)
FROM
    dataset_ref
;
SELECT
    COUNT(*)
FROM
    column_group_ref
;
SELECT
    COUNT(*)
FROM
    column_assoc
;
SELECT
    COUNT(*)
FROM
    column_ref
;
SELECT
    COUNT(*)
FROM
    model_param_group_assoc
;
SELECT
    COUNT(*)
FROM
    model_ref
;
SELECT
    COUNT(*)
FROM
    param_group_ref
;
SELECT
    COUNT(*)
FROM
    param_assoc
;
SELECT
    COUNT(*)
FROM
    param_ref
;
-- Get columns scores and name for dataset name - indiscriminately regarding other criterions
SELECT
    main.score,
    column_ref.name,
    dataset_ref.name
FROM
        main
    JOIN
        serie_ref ON serie_ref.id = main.serie_id
    JOIN
        dataset_column_group_assoc ON dataset_column_group_assoc.id = main.dataset_column_group_id
    JOIN
        dataset_ref ON dataset_ref.id = dataset_column_group_assoc.dataset_id
    JOIN
        column_group_ref ON column_group_ref.id = dataset_column_group_assoc.column_group_id
    JOIN
        column_assoc ON column_assoc.column_group_id = column_group_ref.id
    JOIN
        column_ref ON column_ref.id = column_assoc.column_id
WHERE
    serie_ref.mode = "column"
;




SELECT
        main.score,
        CASE
            WHEN column_assoc.column_group_id >= 0
                THEN (
                    SELECT column_assoc.column_group_id FROM column_assoc
                )
        END AS column_names
    FROM
            main
        JOIN
            serie_ref ON serie_ref.id = main.serie_id
        JOIN
            dataset_column_group_assoc ON dataset_column_group_assoc.id = main.dataset_column_group_id
        JOIN
            dataset_ref ON dataset_ref.id = dataset_column_group_assoc.dataset_id
        JOIN
            column_group_ref ON column_group_ref.id = dataset_column_group_assoc.column_group_id
        JOIN
            column_assoc ON column_assoc.column_group_id = column_group_ref.id
        JOIN
            column_ref ON column_ref.id = column_assoc.column_id
    WHERE
            (serie_ref.mode = 'column')
        AND
            (dataset_ref.name = '{dataset_name}')
    ;