# (Work In Progress) ai-tuner

## Introduction

This project aims to make the simpler, lower quality ai deployment quicker and better by automatically benchmarking and picking machine learning models.

## Requires

Python
pipenv

### Python librairies

requests
pandas
sklearn
dash
pytest

## How to

Clone or download the repository  
Install the Python virtual environment, using following CLI command  
```bash
pipenv install
```  
```bash
pipenv run python run.py <options>
Options:
    Exclusive:
    -d filepath to data (csv or json)
    -D URL to data (csv or json)
```

## Documentation

<a href="https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/data_model.md" target="_blank">&lt;new tab---&gt; &nbsp; &nbsp; Data model &nbsp; &nbsp; &lt;new tab---&gt;</a>  
<a href="https://gitlab.com/Arnaud_Lefebvre/ai-tuner/-/blob/main/docs/tech_design.md" target="_blank">&lt;new tab---&gt; &nbsp; &nbsp; Functions workflow &nbsp; &nbsp; &lt;new tab---&gt;</a>

### Links

#### sklearn

https://scikit-learn.org/stable/

##### DecisionTreeClassifier

https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html?highlight=decisiontree#sklearn.tree.DecisionTreeClassifier

#### DbSchema

https://dbschema.com/

## Copyrights

© 2021 Arnaud Lefebvre de Ladonchamps. This publication is offered for license under the Attribution Share-Alike license of Creative Commons, accessible at https://creativecommons.org/licenses/by-sa/4.0/legalcode and also described in summary form at https://creativecommons.org/licenses/by-sa/4.0/. By utilizing this ai-tuner, you acknowledge and agree that you have read and agree to be bound by the terms of the Attribution Share-Alike license of Creative Commons.